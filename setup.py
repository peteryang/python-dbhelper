#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
-------------------------------------------------
@version    : v1.0 
@author     : fangzheng
@contact    : zfang@hillinsight.com
@software   : PyCharm 
@filename   : setup.py
@create time: 2019/4/18 19:51 
@describe   : 
@use example: python setup.py [param1 param2]
-------------------------------------------------
"""

#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
      name="dbhelper",
      version="1.0",
      keywords=("dbhelper"),
      description="python db helper",
      long_description="python db helper",
      license="MIT Licence",

      url="https://gitee.com/fangzheng0518/python-dbhelper",
      author="dbhelper",
      author_email="founder517518@163.com",

      packages=find_packages(),
      include_package_data=True,
      platforms="any",
      install_requires=[],

      scripts=[],
      entry_points={
            'console_scripts': [
                  'test = test.help:main'
            ]
      }
)