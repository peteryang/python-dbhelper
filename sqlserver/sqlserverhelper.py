#!/usr/bin/python
# -*- coding: utf-8 -*-

""" 
-------------------------------------------------
@version    : v1.0 
@author     : fangzheng
@contact    : zfang@hillinsight.com
@software   : PyCharm 
@filename   : sqlserverhelper.py
@create time: 2019/3/26 22:00 
@describe   : 
@use example: python sqlserverhelper.py [param1 param2]
-------------------------------------------------
"""

from IDBHelper import DBHelper
from logger import Logger
from dbutils import select,execute,DBConfig
import pymssql

log = Logger(name='sqlserverhelper')


class SqlServerHelper(DBHelper):
    """
    SqlServerHelper
    """
    _connect = None

    def __init__(self,db_config):
        """
        Construnctor for SqlServerHelper
        """
        self.init(db_config)

    def init(self, db_config):
        """
        init database connection
        :param dbconfig:
        :return: True/False
        """
        try:
            dbconfig = DBConfig(db_config)
            self._connect = pymssql.connect(host=str(dbconfig.host),
                                            user=str(dbconfig.username),
                                            password=str(dbconfig.password),
                                            database=str(dbconfig.db),
                                            port=dbconfig.port,
                                            charset=str(dbconfig.charset)
                                         )
            log.info(" Connected to SqlServer database [ {db} ]...".format(db=dbconfig.db ))
            return True
        except Exception as e:
            log.error(" Connect SqlServer exception  : \n{e}\n".format(e=str(e)))
            return False


    def get_conn(self):
        if self._connect:
            return self._connect
        else:
            self.init()
            return self._connect

    def close_conn(self):
        if self._connect:
            self._connect.close()
            log.info(" SqlServer database connection closed....")

    def table_is_exist(self,table_name):
        """
        Check table is exist
        :param tablename:
        :return:
        """
        sql = "SELECT count(*) FROM sysobjects WHERE name = '{tname}' ".format(tname=table_name)
        rows = self.select(sql=sql)
        cnt = 0
        if len(rows):
            cnt = rows.pop()[0]

        if cnt >= 1:
            return True
        else:
            return False

    def select(self, sql, param=None , size=None):
        """
        Query data
        :param sql:
        :param param:
        :param size: Number of rows of data you want to return
        :return:
        """
        return select(self._connect,sql,param,size)

    def execute(self,sql, param=None):
        """
        exec DML：INSERT、UPDATE、DELETE
        :param sql: dml sql
        :param param: string|list
        :return: Number of rows affected
        """
        return execute(self._connect, sql, param)




